#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <unistd.h>
#include <math.h>

#include <pthread.h>
#include <semaphore.h>
#include <sys/wait.h>



#ifndef N
#define N 1000
#endif


#define PI 3.14159265358979323846



double quotient = 0.0;
pthread_mutex_t mutex;



void* add_term(void* arg)
{
    unsigned long rank = (unsigned long)((ptrdiff_t)(arg));
    pthread_mutex_lock(&mutex);
    quotient += 1.0/(rank*rank);
    pthread_mutex_unlock(&mutex);
    return NULL;
}



int main(void)
{
    printf("Pi = %lf\n", PI);

    pthread_mutex_init(&mutex, NULL);
    pthread_t threads[N];

    for (size_t i=0; i<N; ++i)
    {
        int creat_ret;
        if ((creat_ret = pthread_create(&threads[i], NULL, add_term, (void*)((ptrdiff_t)(i+1))))) {
            fprintf(stderr, "%s\n", strerror(creat_ret));
        }
    }

    for (size_t i=0; i<N; ++i) {
        pthread_join(threads[i], NULL);
    }

    printf("approx Pi: %lf\n", sqrt(quotient*6.0));

    return EXIT_SUCCESS;
}
