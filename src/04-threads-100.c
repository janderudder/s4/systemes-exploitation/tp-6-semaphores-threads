#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <unistd.h>

#include <pthread.h>
#include <sys/wait.h>

#include <semaphore.h>


#ifndef N_MAX
#define N_MAX 30
#endif



sem_t odd_sem, pair_sem;



void* print_pairs(void*)
{
    for (unsigned n=1; n<=N_MAX; ++n) {
        if (n%2 == 0) {
            sem_wait(&pair_sem);
            printf("%u\n", n);
            fflush(stdout);
            sem_post(&odd_sem);
        }
    }
    return NULL;
}


void* print_odds(void*)
{
    for (unsigned n=1; n<=N_MAX; ++n) {
        if (n%2 != 0) {
            sem_wait(&odd_sem);
            printf("%u\n", n);
            fflush(stdout);
            sem_post(&pair_sem);
        }
    }
    return NULL;
}


int main(void)
{
    pthread_t parent_thread, child_thread;

    sem_init(&odd_sem, 0, 1);
    sem_init(&pair_sem, 0, 0);

    pthread_create(&parent_thread, NULL, print_pairs, NULL);
    pthread_create(&child_thread, NULL, print_odds, NULL);

    pthread_join(parent_thread, NULL);
    pthread_join(child_thread, NULL);

    sem_destroy(&odd_sem);
    sem_destroy(&pair_sem);

    return EXIT_SUCCESS;
}
