#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <unistd.h>


#include <pthread.h>



pthread_mutex_t sum_mutex;
size_t global_sum = 0;




void* threaded_function(void* arg)
{
    const ptrdiff_t p = (ptrdiff_t)(arg);
    const size_t n = (size_t)(p);

    #ifdef VERBOSE

        printf("Thread %lu: attente mutex...\n", n);

        pthread_mutex_lock(&sum_mutex);

        printf("Thread %lu: mutex obtenu.\n", n);

        global_sum += n;

        printf("Thread %lu: libère le mutex.\n", n);

        pthread_mutex_unlock(&sum_mutex);

        printf("Thread %lu: message après libération du mutex.\n", n);

    #else

        pthread_mutex_lock(&sum_mutex);
        global_sum += n;
        pthread_mutex_unlock(&sum_mutex);

    #endif

    return (void*)p;
}




size_t summation(size_t n)
{
    return n * (n+1) / 2;
}




int main(const int argc, char** argv)
{
    if (argc < 2) {
        printf("Veuillez passer le nombre de threads en argument.\n");
        return EXIT_FAILURE;
    }

    const size_t N = atoi(argv[1]);

    if (N < 1) {
        printf("Veuillez passer un nombre de threads supérieur ou égal à 1.\n");
        return EXIT_FAILURE;
    }

    // initialisation du mutex;
    pthread_mutex_init(&sum_mutex, NULL);

    // espace de stockage pour les gestionnaires de threads
    pthread_t* threads = calloc(sizeof(pthread_t), N);

    // création et lancement des threads
    for (size_t i=0; i<N; ++i)
    {
        if (pthread_create(&threads[i], NULL, threaded_function, (void*)((ptrdiff_t)(i)))) {
            perror("thread error");
        }
    }

    // attente de la terminaison de tous les threads
    for (size_t i=0; i<N; ++i)
    {
        pthread_join(threads[i], NULL);
    }

    // libération des ressources
    free(threads);
    pthread_mutex_destroy(&sum_mutex);

    printf("\nRésultat\n---------\n");
    printf("Variable globale: %lu\n", global_sum);
    printf("Sommation de %lu: %lu\n", N-1, summation(N-1));

    return EXIT_SUCCESS;
}
