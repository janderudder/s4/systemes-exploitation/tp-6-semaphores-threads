#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <unistd.h>


#include <pthread.h>




size_t global_sum = 0;




void* threaded_function(void* arg)
{
    return arg;
}




size_t summation(size_t n)
{
    return n * (n+1) / 2;
}




int main(const int argc, char** argv)
{
    if (argc < 2) {
        printf("Veuillez passer le nombre de threads en argument.\n");
        return EXIT_FAILURE;
    }

    const size_t N = atoi(argv[1]);

    if (N < 1) {
        printf("Veuillez passer un nombre de threads supérieur ou égal à 1.\n");
        return EXIT_FAILURE;
    }

    // espace de stockage pour les gestionnaires de threads
    pthread_t* threads = calloc(sizeof(pthread_t), N);

    // création et lancement des threads
    for (size_t i=0; i<N; ++i)
    {
        if (pthread_create(&threads[i], NULL, threaded_function, (void*)((ptrdiff_t)(i)))) {
            perror("thread error");
        }
    }

    // attente de la terminaison de tous les threads
    for (size_t i=0; i<N; ++i)
    {
        void* thread_ret;

        pthread_join(threads[i], &thread_ret);

        global_sum += (size_t)((ptrdiff_t)thread_ret);

        #ifdef VERBOSE
            printf("Le thread %lu a retourné %lu.\n", i, (ptrdiff_t)thread_ret);
        #endif
    }

    free(threads);

    printf("Résultat\n---------\n");
    printf("Variable globale: %lu\n", global_sum);
    printf("Sommation de %lu: %lu\n", N-1, summation(N-1));

    return EXIT_SUCCESS;
}
