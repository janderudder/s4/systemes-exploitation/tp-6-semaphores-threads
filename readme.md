Compile with `-pthread` compiler option.

Example:

```bash
    # run ex 1 with 10 threads and output messages
    ./compile-run.sh src/01-n_threads.c -DVERBOSE -pthread -- 10
```
