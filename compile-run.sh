#!/bin/bash
#
#	Compile (to ./bin/) and run a C source file.
#

# check or create output directory
[[ -d ./bin/ ]] || mkdir 'bin'

# check or set compiler
if [[ -z $CC ]]
then
	echo -n 'CC not set, '
	if [[ $(command -pv gcc >/dev/null) -eq 0 ]]; then
		echo 'fallback to gcc.'
		CC=gcc
	elif [[ $(command -pv clang >/dev/null) -eq 0 ]]; then
		echo 'fallback to clang.'
		CC=clang
	else
		echo 'no compiler found, aborting.'
		exit -1
	fi
fi

# input/output file names
source="${1-src/main.c}"			# src/main.c if no argument provided
output="$(basename "$source")"
output="${output::-2}" 				# remove extension
shift

# handle script arguments
CompilerOptions="-std=c17 -Wall -Wextra"
LinkerOptions=''

# add args until '--' to either compiler or linker options
while [[ $1 && $1 != "--" ]]
do
	if [[ $1 =~ -l.+ ]]; then
		LinkerOptions+=" $1"
	else
		CompilerOptions+=" $1"
	fi
	shift
done

# pass args after '--' to the program
shift

# compile
$CC $CompilerOptions -o bin/"$output" "$source" $LinkerOptions \
	&& ./bin/"$output" "$@"
